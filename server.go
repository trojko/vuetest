package main

import (
	"bitbucket.com/trojko/larTest/api"
	"bitbucket.com/trojko/larTest/models"
	"bitbucket.com/trojko/larTest/routes"
	"github.com/urfave/negroni"
)

func main() {
	db := models.NewSqliteDB("data.db")
	api := api.NewAPI(db)
	routes := routes.NewRoutes(api)
	n := negroni.Classic()
	n.UseHandler(routes)
	n.Run(":3000")
}
