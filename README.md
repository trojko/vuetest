
### To get started:

``` bash
# install Go dependencies (and make sure ports 3000/8080 are open)
go get -u 
go run server.go

# open a new terminal and change to the client dir
cd client

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

